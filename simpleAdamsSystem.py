#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 21:16:14 2021

@author: robert
"""

import FINE as fn
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# 1. Create an energy system model instance 
locations = {'bd1'}
commodityUnitDict = {'electricity': 'kW_el','heatRoom':'kW_th','heatConcrete':'kw_th','coldRoom':'kw_th'}
commodities = {'electricity','heatRoom','heatConcrete','coldRoom'}
numberOfTimeSteps=8760
hoursPerTimeStep=1
esM = fn.EnergySystemModel(locations=locations, commodities=commodities, numberOfTimeSteps=8760,
                           commodityUnitsDict=commodityUnitDict,
                           hoursPerTimeStep=1, costUnit='1e Euro', lengthUnit='km', verboseLogLevel=0)

# 2. Add commodity sources to the energy system model
## 2.1. Electricity sources
### 2.1.1 Electricity purchase from grid
esM.add(fn.Source(esM=esM, name='Electricity purchase', commodity='electricity', hasCapacityVariable=False,
                  commodityCost=0.298))

### 2.1.2 PV
# Input parameters
name, commodity ='PV', 'electricity'
hasCapacityVariable = True
dailyProfileSimple = [0,0,0,0,0,0,0,0.05,0.15,0.2,0.4,0.8,0.7,0.4,0.2,0.15,0.05,0,0,0,0,0,0,0]
operationRateMax = pd.DataFrame([u*np.sin(day*np.pi/365)**10 for day in range(365) for u in dailyProfileSimple],
                                index=range(8760),columns=['bd1'])
capacityMax = 10
investPerCapacity, opexPerCapacity = 1000, 1000*0.02
interestRate, economicLifetime = 0.08, 25

# If data should be read from an excel file:
#writer = pd.ExcelWriter('PV_Profile.xlsx') # writes data to an excel file
#operationRateMax.to_excel(writer)                  # (not required if excel file
#writer.save()                                      #  already exists)
#operationRateMax = pd.read_excel('PV_Profile.xlsx', index_col=0) # reads an excel file located in 
                                                            # the current working directory

# Code
esM.add(fn.Source(esM=esM, name=name, commodity=commodity, hasCapacityVariable=hasCapacityVariable,
    operationRateMax=operationRateMax, capacityMax=capacityMax, investPerCapacity=investPerCapacity,
    opexPerCapacity=opexPerCapacity, interestRate=interestRate, economicLifetime=economicLifetime))


# 3. Add conversion components to the energy system model
### 3.1 Heatpump heatRoom from Source Air
esM.add(fn.ConversionDynamic(esM=esM,
                      name='Heatpump Air',
                      physicalUnit = 'kW_el',
                      commodityConversionFactors={'electricity':-1/2, 'heatRoom':1},
                      hasIsBuiltBinaryVariable=True,
                      hasCapacityVariable=True,
                      rampUpMax=0.5,
                      linkedConversionCapacityID='hp',
                      interestRate = 0.08,
                      economicLifetime = 20,
                      investIfBuilt=2800/3,
                      #divide by 3 since 1/3 of ONE single unit
                      investPerCapacity=725/3,
                      #divide by 3 since 1/3 of ONE single unit
                      opexPerCapacity = 725/3*0.02,
                      #divide by 3 since 1/3 of ONE single unit
                      bigM = 2000))
### 3.2 Heatpump heatRoom from Source Concrete
esM.add(fn.ConversionDynamic(esM=esM,
                      name='Heatpump Concrete',
                      physicalUnit = 'kW_el',
                      commodityConversionFactors={'electricity':-1/6,'heatConcrete':-1,'heatRoom':1},
                      hasIsBuiltBinaryVariable=True,
                      hasCapacityVariable=True,
                      rampUpMax=0.1,
                      linkedConversionCapacityID='hp',
                      interestRate = 0.08,
                      economicLifetime = 20,
                      #divide by 3 since 1/3 of ONE single unit
                      investIfBuilt=2800/3,
                      #divide by 3 since 1/3 of ONE single unit
                      investPerCapacity=725/3,
                      #divide by 3 since 1/3 of ONE single unit
                      opexPerCapacity = 725/3*0.02,
                      bigM = 2000))
### 3.3 Coldpump coldRoom via heatRoom to Sink Concrete
esM.add(fn.ConversionDynamic(esM=esM,
                      name='Coldpump Concrete',
                      physicalUnit = 'kW_el',
                      commodityConversionFactors={'electricity':-1/6,'coldRoom':1,'heatConcrete':1},
                      hasIsBuiltBinaryVariable=True,
                      hasCapacityVariable=True,
                      rampUpMax=0.1,
                      linkedConversionCapacityID='hp',
                      interestRate = 0.08,
                      economicLifetime = 20,
                      #divide by 3 since 1/3 of ONE single unit
                      investIfBuilt=2800/3,
                      #divide by 3 since 1/3 of ONE single unit
                      investPerCapacity=725/3,
                      #divide by 3 since 1/3 of ONE single unit
                      opexPerCapacity = 725/3*0.02,
                      bigM = 2000))
### 3.4 Coldpump optional regeneration of Concrete
esM.add(fn.ConversionDynamic(esM=esM,
                      name='ColdAirpump Concrete',
                      physicalUnit = 'kW_el',
                      commodityConversionFactors={'electricity':-1/4,'heatConcrete':1},
                      hasIsBuiltBinaryVariable=True,
                      hasCapacityVariable=True,
                      rampUpMax=0.1,
                      linkedConversionCapacityID='hp',
                      interestRate = 0.08,
                      economicLifetime = 20,
                      #divide by 3 since 1/3 of ONE single unit
                      investIfBuilt=2800/3,
                      #divide by 3 since 1/3 of ONE single unit
                      investPerCapacity=725/3,
                      #divide by 3 since 1/3 of ONE single unit
                      opexPerCapacity = 725/3*0.02,
                      bigM = 2000))
# 4. Add commodity storages to the energy system model
## 4.1.  Heat
### 4.1.1 Concrete: heatConcrete
esM.add(fn.Storage(esM=esM,
                   name='Thermal Storage Concrete',
                   commodity='heatConcrete',
                   selfDischarge=0.001,
                   hasIsBuiltBinaryVariable=True,
                   capacityMax=7500,
                   capacityMin=1500,
                   #capacityFix=7500,
                   stateOfChargeMin=0.1,
                   interestRate = 0.00,
                   economicLifetime = 50,
                   investIfBuilt=4000,
                   investPerCapacity=0,
                   bigM = 250000))
## 4.2 Electricity
### 4.2.1 Li-Ion
#The self discharge of a lithium ion battery is here described as 3% per month. The self discharge per hours is obtained using the equation (1-$\text{selfDischarge}_\text{hour})^{30*24\text{h}} = 1-\text{selfDischarge}_\text{month}$.
esM.add(fn.Storage(esM=esM, name='Li-ion batteries', commodity='electricity',
                   hasCapacityVariable=True, capacityMax=10, chargeEfficiency=0.95,
                   cyclicLifetime=10000, dischargeEfficiency=0.95, selfDischarge=1-(1-0.03)**(1/(30*24)),
                   chargeRate=1, dischargeRate=1, doPreciseTsaModeling=False,
                   investPerCapacity=0.151, opexPerCapacity=0.002, interestRate=0.08,
                   economicLifetime=10))

# 5. Add commodity sinks to the energy system model
## 5.1. Cold sinks
### Demand: coldRoom
dailyProfileSimple = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]

operationRateFix = pd.DataFrame([[(u+0.1*np.random.rand())*8*np.sin(day*np.pi/365)**10]
                                 for day in range(365) for u in dailyProfileSimple],
                                index=range(8760),columns=['bd1']).round(2)

# If data should be read from an excel file:
#writer = pd.ExcelWriter('demandColdProfile.xlsx') # writes data to an excel file
#operationRateFix.to_excel(writer)                  # (not required if excel file
#writer.save()                                      #  already exists)
#operationRateFix = pd.read_excel('demandColdProfile.xlsx', index_col=0) # reads an excel file located in 
                                                            # the current working directory

esM.add(fn.Sink(esM=esM, name='BuildingsCold', commodity='coldRoom',
                hasCapacityVariable=False, operationRateFix=operationRateFix))

## 5.2 Heat sinks
### 5.2.1 Demand: heatRoom
dailyProfileSimple = [0.0,0.0,1.0,1.0,1.0,1.0,1.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,1.0,1.0,1.0,0.0]
operationRateFix = pd.DataFrame([[(u+0.1*np.random.rand())*2.5]
                                 for day in range(365) for u in dailyProfileSimple],
                                index=range(8760),columns=['bd1']).round(2)

# If data should be read from an excel file:
#writer = pd.ExcelWriter('demandHeatProfile.xlsx') # writes data to an excel file
#operationRateFix.to_excel(writer)                  # (not required if excel file
#writer.save()                                      #  already exists)
#operationRateFix = pd.read_excel('demandHeatProfile.xlsx', index_col=0) # reads an excel file located in 
                                                            # the current working directory

esM.add(fn.Sink(esM=esM, name='BuildingsHeat', commodity='heatRoom',
                hasCapacityVariable=False, operationRateFix=operationRateFix))
# 6. Optimize energy system model
#All components are now added to the model and the model can be optimized. If the computational complexity of the optimization should be reduced, the time series data of the specified components can be clustered before the optimization and the parameter timeSeriesAggregation is set to True in the optimize call.
esM.cluster(numberOfTypicalPeriods=7)
esM.optimize(timeSeriesAggregation=True, solver='glpk')

# 7. Selected results output
### Sources and Sink
#Show optimization summary
print(esM.getOptimizationSummary("SourceSinkModel", outputLevel=2))

#Plot operation time series (either one or two dimensional)
fig1, ax1 = fn.plotOperationColorMap(esM, 'BuildingsHeat', 'bd1')
fig2, ax2 = fn.plotOperation(esM, 'BuildingsCold', 'bd1')
fig3, ax3 = fn.plotOperationColorMap(esM, 'BuildingsCold', 'bd1')
fig4, ax4 = fn.plotOperationColorMap(esM, 'Coldpump Concrete', 'bd1')

### Conversion
#Show optimization summary
print(esM.getOptimizationSummary("ConversionDynamicModel", outputLevel=2))
fig5, ax5 = fn.plotOperationColorMap(esM, 'Heatpump Concrete', 'bd1')
fig6, ax6 = fn.plotOperationColorMap(esM, 'Heatpump Air', 'bd1')

### Storage
#Show optimization summary
print(esM.getOptimizationSummary("StorageModel", outputLevel=2))

fig7, ax7 = fn.plotOperation(esM, 'Thermal Storage Concrete', 'bd1', 
                                   variableName='stateOfChargeOperationVariablesOptimum')
fig8, ax8 = fn.plotOperationColorMap(esM, 'Thermal Storage Concrete', 'bd1', 
                                   variableName='stateOfChargeOperationVariablesOptimum')
fig9, ax9 = fn.plotOperation(esM, 'Li-ion batteries', 'bd1', 
                                   variableName='stateOfChargeOperationVariablesOptimum')
fig10, ax10 = fn.plotOperationColorMap(esM, 'Li-ion batteries', 'bd1', 
                                   variableName='stateOfChargeOperationVariablesOptimum')
fig11, ax11 = fn.plotOperation(esM, 'Electricity purchase', 'bd1')
fig12, ax12 = fn.plotOperationColorMap(esM, 'Electricity purchase', 'bd1')
fig13, ax13 = fn.plotOperation(esM, 'PV', 'bd1')
fig14, ax14 = fn.plotOperationColorMap(esM, 'PV', 'bd1')


